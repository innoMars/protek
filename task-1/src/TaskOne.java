import java.util.Scanner;

public class TaskOne {
    public static void main(String[] args) {
        try(Scanner scanner = new Scanner(System.in)) {
            while (true) {
                int age;
                System.out.println("Сколько вам лет?");
                age = scanner.nextInt();

                if (age >= 18) {
                    System.out.println("Вы достигли совершеннолетия и можете приобрести это лекарство.");
                    break;
                }
                System.out.println("Вы не достигли совершеннолетия и не можете приобрести это лекарство.");
            }
        }catch (NumberFormatException e){
            throw new IllegalArgumentException(e);
        }
    }
}
