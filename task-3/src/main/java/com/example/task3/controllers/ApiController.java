package com.example.task3.controllers;

import com.example.task3.dto.ADto;
import com.example.task3.services.ResponseService;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class ApiController {

    private final ResponseService parseService;

    //  server.port = 80
    @GetMapping("api/v1/getXML")
    public ResponseEntity<ADto> getXMLDocument() {

        Optional<ADto> aDto = parseService.sendJSON();

        if (aDto.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok().body(aDto.get());
    }

    // src/main/resources/request.http тестовый запрос.
    @PostMapping(value = "api/v1/sendJSON", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String transform(@RequestBody ADto request) {
        JSONObject jo = new JSONObject(request);
        return XML.toString(jo);
    }
}
