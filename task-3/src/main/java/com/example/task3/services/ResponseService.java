package com.example.task3.services;

import com.example.task3.dto.ADto;
import com.example.task3.dto.BDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ResponseService {

    public Optional<ADto> sendJSON(){

        BDto bDto1 = BDto.builder()
                .b("Hello First")
                .build();

        BDto bDto2 = BDto.builder()
                .b("Hello Second")
                .build();

        ADto aDto = ADto.builder()
                .a(List.of(bDto1,bDto2))
                .build();


        return Optional.of(aDto);
    }
}
